#importing random numbers from library
from random import randint

#generate random guesses for computer
month_number = randint(1, 12)
year_number = randint(1924, 2004)

#input name of user
user_name = input("Hi! What is your name?")

#generate guesses
for guess_number in range(5):
    guess_number = guess_number + 1
    print("Guess " , guess_number , " : " , user_name , " were  you " , " born  on " , month_number , " / " , year_number , " ?")

    response = input("yes or no?")
    if response == "yes":
        print("I knew it!")
        break
    else:
        if guess_number != 5:
            print("Drat! Lemme try again!")
            month_number = randint(1, 12)
            year_number = randint(1924, 2004)
        else:
            print("I have other things to do. Good bye.")
